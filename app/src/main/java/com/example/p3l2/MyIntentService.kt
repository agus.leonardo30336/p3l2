package com.example.p3l2

import android.app.IntentService
import android.content.Intent
import android.os.IBinder
import android.widget.Toast

class MyIntentService : IntentService("MyIntentService") {
    override fun onHandleIntent(intent: Intent?) {
        val timeDelay = intent!!.getLongExtra("timeDelay", 0L)
        Thread.sleep(timeDelay * 1000)
        val intentMessage = Intent(EXTRA_SLEEP_STATUS)
        sendBroadcast(intentMessage)
    }

    override fun onBind(intent: Intent?): IBinder? {
        TODO("Return the communication channel to the service.")
    }

    override fun onDestroy() {
        Toast.makeText(this, "Service stop", Toast.LENGTH_SHORT).show()
    }
}