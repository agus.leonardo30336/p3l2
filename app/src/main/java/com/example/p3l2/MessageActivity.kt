package com.example.p3l2

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_message.*

class MessageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)
        val timeDelay = intent.getStringExtra("timeDelay")
        val message = intent.getStringExtra("message")
        waktu.text = "Anda mendapatkan pesan $timeDelay detik yang lalu"
        pesan.text = "Isi Pesan : $message"
    }
}