package com.example.p3l2

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

const val EXTRA_SLEEP_STATUS = "sleep_status"
class MainActivity : AppCompatActivity() {

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (Settings.System.getInt(context!!.contentResolver, Settings.Global.AIRPLANE_MODE_ON, 0) == 0) {
                val intentMessage = Intent(this@MainActivity, MessageActivity::class.java)
                intentMessage.putExtra("message", pesan.text.toString())
                intentMessage.putExtra("timeDelay", timeDelay.text.toString())
                startActivity(intentMessage)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val filterReceiver = IntentFilter(EXTRA_SLEEP_STATUS)
        registerReceiver(broadcastReceiver, filterReceiver)

        send.setOnClickListener {
            if (Settings.System.getInt(this.contentResolver, Settings.Global.AIRPLANE_MODE_ON, 0) == 0) {
                val intentService = Intent(this, MyIntentService::class.java)
                intentService.putExtra("timeDelay", Integer.parseInt(timeDelay.text.toString()).toLong())
                intentService.putExtra("message", pesan.text.toString())
                Toast.makeText(
                    this,
                    "Pesan anda : ${pesan.text} \nakan ditunda pengirimannya dalam ${timeDelay.text} detik",
                    Toast.LENGTH_SHORT
                ).show()
                startService(intentService)

            } else
                Toast.makeText(
                    this,
                    "Pesan tidak dapat dikirim\nAnda berada dalam FlightMode",
                    Toast.LENGTH_SHORT
                ).show()
        }
    }
}
